import * as mongoose from "mongoose";
import { ThreadSchema } from "../models/Thread";
import { Request, Response } from "express";

const Thread = mongoose.model("Thread", ThreadSchema);

export default class ThreadController {
    public getThreads(req: Request, res: Response) {
        console.log("all");
        Thread.find((err, threads) => {
            res.send(threads);
        })
    }

    public getThreadById(req: Request, res: Response) {
        console.log("get");
        Thread.findById(req.params.threadId, (err, thread) => {
            if (err) res.send(err);
            res.send(thread);
        })
    }

    public addNewThread(req: Request, res: Response) {
        console.log("add");
        let thread = new Thread(req.body);

        Thread.save((err, thread) => {
            if(err) res.send(err);
            res.json(thread);
        })
    }

    //---as

    public saveThread(req: Request, res: Response) {
        console.log("save");
        Thread.findOneAndUpdate({ threadId: req.params.threadId }, req.body, { new: true, upsert: true },
            (err, thread) => {
                if (err) res.send(err);
                res.send(thread);
            })
    }

    public deleteThread(req: Request, res:Response) {
        console.log("del");
        Thread.remove({_id: req.params.threadId}, (err, thread) => {
            if(err) res.send(err);
            res.json({message: "Successfully deleted thread!"});
        })
    }
}
