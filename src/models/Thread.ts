import * as mongoose from "mongoose";

const Schema = mongoose.Schema;

export const ThreadSchema = new Schema({
    title: String,
    threadId: Number,
    url: String,
    preview: String,
    reads: Number,
    answers: Number,
    op: String,
    last_message: Date,
    record_date: {
        type: Date,
        default: Date.now
    }
})

export default ThreadSchema;