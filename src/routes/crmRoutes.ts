import {Request, Response} from "express";
import ThreadController from "../controllers/ThreadController";

export class Routes {
    public threadController: ThreadController = new ThreadController();

    public routes(app): void {
        app.route("/").get((req: Request, res: Response) => {
            res.status(200).send({
                message: "Get request successful!"
            })
        })
        app.route("/threads").get(this.threadController.getThreads);
        app.route("/thread/:threadId")
            .get(this.threadController.getThreadById)
            .post(this.threadController.addNewThread)
            .put(this.threadController.saveThread)
            .delete(this.threadController.deleteThread);
    }
}